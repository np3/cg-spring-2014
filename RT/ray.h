#pragma once
#include <glm/glm.hpp>

inline void vec4to3(glm::vec3&a, const glm::vec4&b) {
  memcpy(&a, &b, 3 * sizeof(float));
}
inline void vec3to4(glm::vec4&a, const glm::vec3&b) {
  memcpy(&a, &b, 3 * sizeof(float));
}

class Ray
{
public:
  // Constructs a ray
  Ray(const glm::vec3& mOrigin = glm::vec3(), const glm::vec3& direction = glm::vec3())
    : org(mOrigin), dir(glm::normalize(direction))
  {
  }
  ~Ray() {}

  glm::vec3 Apply(float t) const
  {
    return org + dir * t;
  }

  const glm::vec3& Origin() const { return org; }
  const glm::vec3& Dir() const { return dir; }
  Ray ApplyMat(const glm::mat4x4 &tr) const {
    Ray r;
    glm::vec4 _org;
    glm::vec4 _dir;
    vec3to4(_org, org);
    vec3to4(_dir, dir);
    _org.w = 1.0f; // point
    _dir.w = 0.0f; // direction
    _org = tr * _org;
    _dir = tr * _dir;
    vec4to3(r.org, _org);
    vec4to3(r.dir, _dir);
    r.dir = glm::normalize(r.dir);
    return r;
  }
private:
  glm::vec3 org;
  glm::vec3 dir;
};