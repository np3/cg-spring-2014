#ifndef _VEC_H_
#define _VEC_H_
#include "common.h"

template<typename T>
struct Vector3D
{
  T x, y, z;

  Vector3D() : x((T)0), y((T)0), z((T)0) {}
  Vector3D(const T &nx, const T &ny, const T &nz ) : x(nx), y(ny), z(nz)
  {
  }
  Vector3D operator+(const Vector3D &V) const
  {
    return Vector3D(x + V.x, y + V.y, z + V.z);
  }
  Vector3D& operator+=(const Vector3D &V)
  {
    x += V.x, y += V.y, z += V.z;
    return *this;
  }
  Vector3D operator-(const Vector3D &V) const
  {
    return Vector3D(x - V.x, y - V.y, z - V.z);
  }
  Vector3D& operator-()
  {
    x = -x;
    y = -y;
    z = -z;
    return *this;
  }
  Vector3D& operator-=(const Vector3D &V)
  {
    x -= V.x, y -= V.y, z -= V.z;
    return *this;
  }
  Vector3D operator*(double N) const
  {
    return Vector3D(x * N, y * N, z * N);
  }
  Vector3D& operator*=(double N)
  {
    x *= N, y *= N, z *= N;
    return *this;
  }
  Vector3D operator/(double N) const
  {
    return Vector3D(x / N, y / N, z / N);
  }
  Vector3D& operator/=(double N)
  {
    x /= N, y /= N, z /= N;
    return *this;
  }
  double Dot(const Vector3D &V) const
  {
    return x * V.x + y * V.y + z * V.z;
  }
  Vector3D Cross(const Vector3D &V) const
  {
    return Vector3D(y * V.z - V.y * z, 
                 z * V.x - V.z * x, 
                 x * V.y - V.x * y);
  }
  double Length() const
  {
    double sqrLen = Dot(*this);
    return sqrt(sqrLen);
  }
  Vector3D& Normalize()
  {
    double len = Length();
    *this /= len;
    return *this;
  }
  bool operator<(const Vector3D &rhs) {
    return x < rhs.x && y < rhs.y && z < rhs.z;
  }
};

typedef Vector3D<double> Vec3D;
#endif /* _VEC_H_ */