#pragma once
#define GLM_FORCE_RADIANS
#include "ray.h"

struct CameraParams
{
  float fovX;
  float fovY;
  glm::vec3 pos;
  glm::vec3 orientation;
};

class Camera
{
private:
  // camera position
  glm::vec3 pos;
  // camera basis
  glm::vec3 right;
  glm::vec3 up;
  glm::vec3 dir;
  // camera parameters
  float fieldOfViewX;
  float fieldOfViewY;
  float aspectRatio;
  glm::vec3 focusDistance;
  int renderPlaneW;
  int renderPlaneH;
  float invRenderPlaneWFloat;
  float invRenderPlaneHFloat;
public:
  Camera();
  void ApplyCameraParams(const CameraParams &cp);
  void LookAt(const glm::vec3& pos, const glm::vec3& at, const glm::vec3& up);
  void SetFieldOfView(float angleDegX, float angleDegY);
  Ray  CastRay(int x, int y) const; // primary ray
  void SetRenderPlaneDim(int W, int H);
  int GetRenderPlaneH() const { return renderPlaneH; }
  int GetRenderPlaneW() const { return renderPlaneW; }
};