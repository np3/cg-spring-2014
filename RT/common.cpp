#if 0
#include "common.h"

double common::Clamp(double v, const double &a, const double &b)
{
  if (v < a) {
    v = a;
  }
  if (v > b) {
    v = b;
  }
  return v;
}
double common::Saturate(double v)
{
  return Clamp(v, 0.0, 1.0);
}
#endif