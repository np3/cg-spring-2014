#pragma once
#include "ray.h"

struct AABB
{
  bool Intersects(const Ray &R);
  glm::vec3 min, max;
};