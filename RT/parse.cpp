#if 0
#include "parse.h"

#pragma warning(disable:4996)

using namespace std;

typedef multimap<char, string> ParamSet;

vector<Shape *> GetObjects(const Config& C)
{
  vector<Shape *> S;
  Sphere *t1;
  Plane *t2;

  if(C.IsReady() == false)
    return S;
  void *p = C.GetVariables();
  pair<ParamSet::iterator, ParamSet::iterator> pairs;
  pairs = (static_cast<ParamSet *>(p))->equal_range('S');
  for(ParamSet::iterator it = pairs.first; it != pairs.second; it++)
  {
    t1 = new Sphere();
    char *t = const_cast<char *>((*it).second.c_str());
    while(*t == ' ' || *t == 'S')
      t++;
    if(sscanf(t, "%lf %lf %lf %lf %lf %lf %lf %lf %lf", 
      &t1->GetPos().GetX(), &t1->GetPos().GetY(), 
      &t1->GetPos().GetZ(), &t1->GetR(), &t1->GetMaterial().GetR(), 
      &t1->GetMaterial().GetG(), &t1->GetMaterial().GetB(), 
      &t1->GetMaterial().GetRefl(), &t1->GetMaterial().GetShine()) != 9)
      continue;  
    S.insert(S.end(), t1);

  }

  pairs = (static_cast<ParamSet *>(p))->equal_range('P');
  for(ParamSet::iterator it = pairs.first; it != pairs.second; it++)
  {
    t2 = new Plane();
    char *t = const_cast<char *>((*it).second.c_str());
    while(*t == ' ' || *t == 'P')
      t++;
    if(sscanf(t, "%lf %lf %lf %lf %lf %lf %lf %lf %lf", 
      &t2->GetN().GetX(), &t2->GetN().GetY(), &t2->GetN().GetZ(),
      &t2->GetD(), &t2->GetMaterial().GetR(), &t2->GetMaterial().GetG(),
      &t2->GetMaterial().GetB(), &t2->GetMaterial().GetRefl(),
      &t2->GetMaterial().GetShine()) != 9)
      continue;  
    S.insert(S.end(), t2); 
 
  }

  return S;

}

vector<Vec> GetLights(const Config& C)
{
  vector<Vec> S;
  Vec temp;
  if(C.IsReady() == false)
    return S;
  void *p = C.GetVariables();
  pair<ParamSet::iterator, ParamSet::iterator> pairs;
  pairs = (static_cast<ParamSet *>(p))->equal_range('L');
  for(ParamSet::iterator it = pairs.first; it != pairs.second; it++)
  {
    char *t = const_cast<char *>((*it).second.c_str());
    while(*t == ' ' || *t == 'L')
      t++;
    if(sscanf(t, "%lf %lf %lf", &temp.GetX(), &temp.GetY(), &temp.GetZ()) != 3)
      continue;
    S.insert(S.end(), temp);
  }
  return S;
}

Camera GetCamera(const Config& C)
{
  Camera temp;
  Vec nLoc, nAt, nUp;
  if(C.IsReady() == false)
    return temp;
  void *p = C.GetVariables();
  pair<ParamSet::iterator, ParamSet::iterator> pairs;
  pairs = (static_cast<ParamSet *>(p))->equal_range('C');
  ParamSet::iterator it = pairs.first;
  char *t = const_cast<char *>((*it).second.c_str());
  
  while(*t == ' ' || *t == 'C')
    t++;
  if(sscanf(t, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", &temp.GetWs(),
    &temp.GetHs(), &temp.GetWp(), &temp.GetHp(), 
    &temp.GetProjDist(),  &nLoc.GetX(), &nLoc.GetY(), &nLoc.GetZ(),
    &nAt.GetX(), &nAt.GetY(), &nAt.GetZ(),
    &nUp.GetX(), &nUp.GetY(), &nUp.GetZ()) != 14)
    return temp;
  
  temp.SetPos(nLoc, nAt, nUp);
  return temp;

}
#endif