#include "objloader.h"
#include "mesh.h"
#include <fstream>
#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <sstream>
#include "aabb.h"

using namespace std;
using namespace glm;

void readVec3(std::string &str, vec3 &v)
{
  string val0 = str.substr(0, str.find(' '));
  v.x = (float)strtod(val0.c_str(), NULL);

  string val1 = str.substr(val0.length() + 1, str.find(' '));
  v.y = (float)strtod(val1.c_str(), NULL);

  string val2 = str.substr(str.find_last_of(' ') + 1);
  v.z = (float)strtod(val2.c_str(), NULL);
}

void splitString(const std::string &str, std::vector<string> &tokens, char delim, bool skipEmpty)
{
  istringstream ss(str);

  tokens.clear();
  while(!ss.eof()) {
    string x;
    getline(ss, x, delim);
    if (skipEmpty && x.empty()) {
      continue;
    }
    tokens.push_back(x);
  }
}

void parseIndices(const std::string &s, UINT *position, UINT *normal, UINT *texCoord)
{
  std::vector<std::string> tokens;
  splitString(s, tokens, '/', true);

  UINT* pInd[] = {position, normal, texCoord};
  for (int i = 0; i < 3; ++i) {
    *pInd[i] = -1;
  }
  for (unsigned int i = 0; i < tokens.size(); ++i) {
    *pInd[i] = (UINT)strtoul(tokens[0].c_str(), NULL, 10);
  }
}

Mesh* ObjLoader::createMesh(const char *pFileName)
{
  ifstream input(pFileName);
  if (!input.is_open()) {
    return NULL;
  }
  string str;
  vector<vec3> positions;
  vector<vec3> normals;

  struct Vertex {
    vec3 p;
    vec3 n;
    Vertex() : p(vec3(0)), n(vec3(0, 1, 0)) {}
  };
  vector<Vertex> faceVertices;
  vector<UINT> faceVerticesIndices;
  vector<UINT> faceIndices;

  // Result indices for triangles construction
  vector<UINT> indices;
  // Result vertices for triangles construction
  vector<Vertex> vertices;


  while(getline(input, str)) {
    if (str[0] == '#') { // skip comments
      continue;
    }
    if (str[0] == 'v' && str[1] == ' ') {
      vec3 pos;
      str = str.substr(2);
      readVec3(str, pos);
      positions.push_back(pos);
    }
    else if (str[0] == 'v' && str[1] == 'n' && str[2] == ' ') {
      vec3 n;
      str = str.substr(3);
      readVec3(str, n);
      normals.push_back(n);
    }
    else if (str[0] == 'f' && str[1] == ' ') {
      faceVertices.clear();
      faceVerticesIndices.clear();

      std::vector<string> tokens;
      str = str.substr(2);
      splitString(str, tokens, ' ', true);
      for (std::string &s : tokens)
      {
         UINT position, normal, texCoord;

         parseIndices(s, &position, &normal, &texCoord);
         Vertex v;

         if (!positions.empty() && position >= 0) {
           v.p = positions[position - 1];
         }
         if (!normals.empty() && normal >= 0) {
           v.n = positions[normal - 1];
         }
         faceVertices.push_back(v);
         faceVerticesIndices.push_back(position);
      }
      UINT count = faceVertices.size();
      faceIndices.resize(count);

      for (UINT idx = 0; idx < count; ++idx) {
        // Triangle strip
        if (idx > 2) {
          indices.push_back(faceIndices[0]);
          indices.push_back(faceIndices[idx - 1]);
        }
        faceIndices[idx] = vertices.size();
        vertices.push_back(faceVertices[idx]);
        indices.push_back(faceIndices[idx]);
      }
    }
  }
  // little trick to avoid useless copying
  Mesh *pMesh = new Mesh(NULL);
  vector<Triangle *> &faces = pMesh->faces;
  for (UINT idx = 0; idx < indices.size(); idx += 3) {
    const Vertex& a = vertices[indices[idx]];
    const Vertex& b = vertices[indices[idx + 1]];
    const Vertex& c = vertices[indices[idx + 2]];

    faces.push_back(new TriangleSmooth(a.p, b.p, c.p, a.n, b.n, c.n, NULL));
  }
  AABB& aabb = pMesh->aabb;
  vec3 minV(INFINITY, INFINITY, INFINITY), maxV(0);

  for (UINT i = 0; i < positions.size(); ++i) {
    const auto& position = positions[i];

    if (position.x < minV.x) {
      minV.x = position.x;
    }
    if (position.y < minV.y) {
      minV.y = position.y;
    }
    if (position.z < minV.z) {
      minV.z = position.z;
    }

    if (position.x > maxV.x) {
      maxV.x = position.x;
    }
    if (position.y > maxV.y) {
      maxV.y = position.y;
    }
    if (position.z > maxV.z) {
      maxV.z = position.z;
    }
  }
  aabb.max = maxV;
  aabb.min = minV;

  return pMesh;
}