#include "shape.h"
#include "common.h"
#include "solver.h"
#include <cmath>

using namespace glm;

void Shape::ApplyTransform(const ShapeTransform& tr) {
  // Calc ray as p = T^(-1) * p', where p' is a ray for transformed object
  inverseTransform = glm::inverse(tr.transformMatrix);
  // For correct normal calculation
  inverseTransformTransposed = glm::transpose(inverseTransform);
}

inline int SolveEq(const glm::vec3 &c, glm::vec2 &roots, bool straightforward = false)
{
  float D, divider;

  if (straightforward) {
    D = c.y * c.y - 4.f * c.x * c.z;
  } else {
    D = c.y * c.y - c.x * c.z;
  }

  if (D < 0) {
    return 0;
  }

  D = ::sqrt(D);

  if (straightforward) {
    divider = 1.f / (2 * c.x);
  } else {
    divider = 1.f / c.x;
  }

  roots.x = (-c.y - D) * divider;
  roots.y = (-c.y + D) * divider;

  // D = 0
  if (abs(D) < EPS) {
    return 1;
  }

  return 2;
}

inline float GetClosestPoint(const glm::vec2 &roots) {
  float res = INFINITY;

  if (roots.x > 0.0f) {
    res = roots.x;
  }
  if (roots.y > 0.0f) {
    if (roots.y < res) {
      res = roots.y;
    }
  }
  return res;
}

RayIntersection Sphere::FindIntersection(const Ray &_R)
{
  Ray R = _R.ApplyMat(InvTransform());
  vec3 cToOrg = R.Origin() - C;
  vec3 factors;

  factors.x = 1.0f;
  factors.y = dot(R.Dir(), cToOrg);
  factors.z = dot(cToOrg, cToOrg) - Rad * Rad;
  vec2 roots;
  int numRoots = SolveEq(factors, roots);
  if (numRoots == 0) {
    return RayIntersection(false);
  }
  float dist = GetClosestPoint(roots);
  if (dist > 0.0f) {
    RayIntersection rI(true);

    rI.hitShape = this;
    rI.dist = dist;
    rI.normal = GetNormal(R, dist);
    memcpy_s(&rI.localNormal, sizeof(glm::vec3), &rI.normal, sizeof(glm::vec3));
    TransformNormal(rI.normal);
    return rI;
  }
  return RayIntersection(false);
}

vec3 Sphere::GetNormal(const Ray &R, float dist)
{
  return normalize((R.Apply(dist) - C) / Rad);
}

RayIntersection Plane::FindIntersection(const Ray &_R)
{
  Ray R = _R.ApplyMat(InvTransform());
  float dp = dot(R.Dir(), N);
  float dist = -(dot(N, R.Origin() + D)) / dp;

  if (abs(dp) < EPS || dist < 0.f) {
    return RayIntersection(false);
  }

  RayIntersection result(true);
  result.dist = dist;
  result.hitShape = this;
  result.normal = GetNormal(R, dist);
  memcpy_s(&result.localNormal, sizeof(glm::vec3), &result.normal, sizeof(glm::vec3));
  TransformNormal(result.normal);
  return result;
}

vec3 Plane::GetNormal(const Ray &R, float dist)
{
  return N;
}

RayIntersection Triangle::FindIntersection(const Ray &_R)
{
  const float trEPS = 1e-4f * EPS;
  Ray R = _R.ApplyMat(InvTransform());
  vec3 u, v, n;              // triangle vectors
  vec3 dir, w0, w;           // ray vectors
  vec3 pointOfIntersection;
  float r, a, b;              // params to calc ray-plane intersect

  // get triangle edge vectors and plane normal
  u = vertB - vertA;
  v = vertC - vertA;
  n = cross(u, v);              // cross product
  if (n == vec3(0))             // triangle is degenerate
    return RayIntersection(false);                 // do not deal with this case

  dir = R.Dir();              // ray direction vector
  w0 = R.Origin() - vertA;
  a = -dot(n, w0);
  b = dot(n, dir);
  if (abs(b) < trEPS) {     // ray is  parallel to triangle plane
    if (abs(a) < trEPS) {                // ray lies in triangle plane
      return RayIntersection(false);
    }
    else {
      return RayIntersection(false);              // ray disjoint from plane
    }
  }

  // get intersect point of ray with triangle plane
  r = a / b;
  if (r < 0.0) {                   // ray goes away from triangle
    return RayIntersection(false); // => no intersect
  }
  // for a segment, also test if (r > 1.0) => no intersect


  pointOfIntersection = R.Origin() + r * dir;            // intersect point of ray and plane

  // is I inside T?
  float    uu, uv, vv, wu, wv, D;
  uu = dot(u,u);
  uv = dot(u,v);
  vv = dot(v,v);
  w = pointOfIntersection - vertA;
  wu = dot(w,u);
  wv = dot(w,v);
  D = uv * uv - uu * vv;

  // get and test parametric coords
  float s, t;
  s = (uv * wv - vv * wu) / D;
  if (s < 0.0 || s > 1.0)         // I is outside T
    return RayIntersection(false);
  t = (uv * wu - uu * wv) / D;
  if (t < 0.0 || (s + t) > 1.0)  // I is outside T
    return RayIntersection(false);

  // Cache s and t values for normals smoothing
  S = s;
  T = t;

  // I is in T
  RayIntersection result(true);
  result.dist = r;
  result.hitShape = this;
  result.normal = GetNormal(R, result.dist);
  memcpy_s(&result.localNormal, sizeof(glm::vec3), &result.normal, sizeof(glm::vec3));
  TransformNormal(result.normal);

  return result;
}

vec3 Triangle::GetNormal(const Ray&R, float dist)
{
  return N;
}

RayIntersection TriangleSmooth::FindIntersection(const Ray &R)
{
  RayIntersection res = Triangle::FindIntersection(R);
  if (res.exists) {
    res.normal = normalize(S * normB + T * normC + (1.0f - S - T) * normA);
    memcpy_s(&res.localNormal, sizeof(glm::vec3), &res.normal, sizeof(glm::vec3));
    TransformNormal(res.normal);
  }
  return res;
}

RayIntersection Cylinder::FindIntersection(const Ray &_R)
{
  Ray R = _R.ApplyMat(InvTransform());
  const vec3 &origin = R.Origin(), &direction = R.Dir();

  vec3 originToBottom = toBottom(origin);

  vec3 u = direction - axis * (dot(direction, axis));
  vec3 v = originToBottom - axis * (dot(originToBottom, axis));

  // Solve equation c.x * x^2 + c.y * x + c.z = 0
  vec3 c;
  c.x = dot(u, u);
  vec2 roots;
  float distance = -1;

  if (abs(c.x) > EPS)
  {
    c.y = 2 * dot(u, v);
    c.z = dot(v, v) - radius * radius;

    int num_roots = SolveEq(c, roots, true);

    if (num_roots == 0) {
      return RayIntersection(false);
    }

    if (roots.x > 0)
    {
      vec3 tb = toBottom(R.Apply(roots.x)), tt = toTop(R.Apply(roots.x));
      if (dot(axis, tb) > 0 && dot(axis, tt) < 0) {
        distance = roots.x;
      }
    }

    if (roots.y > 0)
    {
      vec3 tb = toBottom(R.Apply(roots.y)), tt = toTop(R.Apply(roots.y));
      if (dot(axis, tb) > 0 && dot(axis, tt) < 0) {
        if (distance < 0 || roots.y < distance) {
          distance = roots.y;
        }
      }
    }
  }
  else
  {
    distance = -1;
  }

  RayIntersection res = RayIntersection(true);

  float axisDotDirection = dot(axis, direction);
  if (fabs(axisDotDirection) < EPS)
  {
    if (distance > 0)
    {
      res.dist = distance;
      res.hitShape = this;
      res.normal	 = GetNormal(R, distance);
      memcpy_s(&res.localNormal, sizeof(glm::vec3), &res.normal, sizeof(glm::vec3));
      TransformNormal(res.normal);
      return res;
    }

    return RayIntersection(false);
  }

  // Bottom
  float axisDotOrigin = dot(axis, origin);
  float centerToOriginDotAxis = dot(originToBottom, axis);
  roots.x = -centerToOriginDotAxis / axisDotDirection;
  if (roots.x > 0)
  {
    vec3 tb = toBottom(R.Apply(roots.x));
    if (dot(tb, tb) < radius * radius) {
      if (distance < 0 || roots.x < distance) {
        distance = roots.x;
      }
    }
  }

  // Top
  float topCenterToOriginDotAxis = dot(origin - topCenter, -axis);
  roots.x = topCenterToOriginDotAxis / axisDotDirection;
  if (roots.x > 0)
  {
    vec3 tt = toTop(R.Apply(roots.x));
    if (dot(tt, tt) < radius * radius) {
      if (distance < 0 || roots.x < distance) {
        distance = roots.x;
      }
    }

  }

  if (distance > 0)
  {
    res.dist = distance;
    res.hitShape = this;
    res.normal	 = GetNormal(R, distance);
    memcpy_s(&res.localNormal, sizeof(glm::vec3), &res.normal, sizeof(glm::vec3));
    TransformNormal(res.normal);
    return res;
  }

  return RayIntersection(false);
}

vec3 Cylinder::GetNormal(const Ray &R, float dist)
{
  vec3 p = R.Apply(dist); // Point of intersection

  // Bottom
  vec3 tb = toBottom(p);
  if (fabs(dot(axis, tb)) < EPS && dot(tb, tb) < radius * radius) {
    return -axis;
  }

  // Top
  vec3 tt = toTop(p);
  if (abs(dot(axis, tt)) < EPS && dot(tt, tt) < radius * radius) {
    return axis;
  }

  return normalize(p - axis * dot(tb, axis) - bottomCenter);
}

RayIntersection Cone::FindIntersection(const Ray &_R)
{
  Ray R = _R.ApplyMat(InvTransform());
  const vec3 &origin = R.Origin(), &direction = R.Dir();

  vec3 originToBottom = toBottom(origin);

  float dirDotAxis = dot(direction, axis);
  float CODotAxis	 = dot(originToBottom, axis);

  vec3 u = direction + axis * (-dirDotAxis);
  vec3 v = originToBottom + axis * (-CODotAxis);
  float w = CODotAxis * radiusDecrease;

  float	radPerDir = dirDotAxis * radiusDecrease;

  // Solve equation c.x * x^2 + c.y * x + c.z = 0
  vec3 c;
  c.x = dot(u, u) - radPerDir * radPerDir;
  vec2 roots;
  float distance = -1;
  float rayExit = -1.f; // Distance, where ray has exited the cone
  if (abs(c.x) > EPS)
  {
    c.y = 2 * (dot(u, v) - w * radPerDir);
    c.z = dot(v, v) - w * w;

    int num_roots = SolveEq(c, roots, true);

    if (num_roots == 0) {
      return RayIntersection(false);
    }

    if (roots.x > 0)
    {
      vec3 tb = toBottom(R.Apply(roots.x)), tt = toTop(R.Apply(roots.x));
      if (dot(axis, tb) > 0 && dot((-axis), tt) > 0) {
        distance = roots.x;
      }
    }
    if (roots.y > 0.f)
    {
      vec3 tb = toBottom(R.Apply(roots.y)), tt = toTop(R.Apply(roots.y));
      if (dot(axis, tb) > 0 && dot(-axis, tt) > 0) {
        if (distance < 0 || roots.y < distance) {
          distance = roots.y;
        }
      }
    }
  }


  RayIntersection res = RayIntersection(true);

  if (abs(dirDotAxis) < EPS)
  {
    if (distance > 0)
    {
      res.dist = distance;
      res.hitShape = this;
      res.normal	 = GetNormal(R, distance);
      memcpy_s(&res.localNormal, sizeof(glm::vec3), &res.normal, sizeof(glm::vec3));
      TransformNormal(res.normal);
      return res;
    }

    return RayIntersection(false);
  }

  roots.x = (dot(-axis, toTop(origin))) / dirDotAxis;
  if (roots.x > 0)
  {
    vec3 tt = toTop(R.Apply(roots.x));
    if (dot(tt, tt) < radius * radius)
    {
      if (distance < 0 || roots.x < distance)
      {
        distance = roots.x;
      }
    }
  }

  if (distance > 0)
  {
    res.dist = distance;
    res.hitShape = this;
    res.normal	 = GetNormal(R, distance);
    memcpy_s(&res.localNormal, sizeof(glm::vec3), &res.normal, sizeof(glm::vec3));
    TransformNormal(res.normal);
    return res;
  }

  return RayIntersection(false);
}

vec3 Cone::GetNormal(const Ray &R, float dist)
{
  vec3 p = R.Apply(dist); // Point of intersection
  vec3 tb = toTop(p);
  if (abs(dot(axis, tb)) < EPS && dot(tb, tb) < radius * radius)
  {
    return axis;
  }
  vec3 approxNorm = p - (axis * (dot(toTop(p), axis)) + topCenter);
  return normalize(approxNorm + axis * (-radiusDecrease * length(approxNorm)));
}

RayIntersection Torus::FindIntersection(const Ray &_R)
{
  Ray R = _R.ApplyMat(InvTransform());
  vec3 o = R.Origin() - pos;
  const vec3& d = R.Dir();
  float a = dot(d, d);
  float b = 2 * dot(o, d);
  float c = dot(o, o) - (innerRad * innerRad) - (outerRad * outerRad);
  std::vector<float> factors;
  factors.push_back(a * a);
  factors.push_back(2 * a * b);
  factors.push_back((b * b) + (2 * a * c) +
    (4 * outerRad * outerRad * d.y * d.y));
  factors.push_back((2 * b * c) +
    (8 * outerRad * outerRad * o.y * d.y));
  factors.push_back((c * c) + (4 * outerRad * outerRad * o.y * o.y) -
    (4 * outerRad * outerRad * innerRad * innerRad));
  std::vector<float> solutions;

  Solver::SolveQuartic(factors, solutions);

  if (solutions.empty()) {
    return RayIntersection(false);
  }
  float dist = INFINITY;
  for (int i = 0; i < (int)solutions.size(); ++i) {
    dist = min(dist, solutions[i]);
  }
  RayIntersection res(true);

  res.dist = dist;
  res.hitShape = this;
  res.normal = GetNormal(R, dist);
  memcpy_s(&res.localNormal, sizeof(glm::vec3), &res.normal, sizeof(glm::vec3));
  TransformNormal(res.normal);
  return res;
}

vec3 Torus::GetNormal(const Ray &R, float dist)
{
  vec3 p = R.Apply(dist);
  vec3 dp = p - pos;
  float mul = dot(dp, dp) - (innerRad * innerRad) - (outerRad * outerRad);
  vec3 normal = dp * (mul * 4);
  normal.y += 8 * outerRad * outerRad * dp.y;
  return normalize(normal);
}