#include "camera.h"
#include "common.h"
#include <cmath>
#include <glm/gtx/rotate_vector.hpp>

using namespace glm;
Camera::Camera() {
  pos = vec3(0);
  right = vec3(1.0f, 0.f, 0.f);
  up = vec3(0.f, 1.0f, 0.0f);
  dir = vec3(0.0f, 0.0f, 1.0f);
}
void Camera::LookAt(const vec3& _pos, const vec3& at, const vec3& _up) {
  dir = normalize(at - pos);
  right = normalize(cross(dir, _up));
  up = cross(right, dir);
  pos = _pos;
}

void Camera::SetRenderPlaneDim(int W, int H) {
  renderPlaneW = W, renderPlaneH = H;
  invRenderPlaneWFloat = 1.0f / (float)renderPlaneW;
  invRenderPlaneHFloat = 1.0f / (float)renderPlaneH;
  aspectRatio = (float)renderPlaneW / (float)renderPlaneH;
}

void Camera::SetFieldOfView(float angleDegX, float angleDegY) {
  fieldOfViewX = angleDegX;
  fieldOfViewY = angleDegY;
  float angleRadX = Deg2Rad(angleDegX);
  float angleRadY = Deg2Rad(angleDegY);
  focusDistance.x = 1.0f / tan(angleRadX * 0.5f);
  focusDistance.y = tan(angleRadY * 0.5f);
  focusDistance.z = 1.0f;
}

Ray Camera::CastRay(int x, int y) const {
  float pX = (2.0f * ((float)x * invRenderPlaneWFloat - 0.5f) * aspectRatio);
  float pY = (2.0f * (0.5f - (float)y * invRenderPlaneHFloat));
  const vec3 &org = pos;
  vec3 direction = right * pX + up * pY + dir * focusDistance.x;

  return Ray(org, direction);
}

void Camera::ApplyCameraParams(const CameraParams &cp)
{
   pos = cp.pos;
   // Setup camera orientation
   // h - heading - rotate around Z(0, 1, 0)
   // p - pitch - rotate around X(1, 0, 0)
   // r - roll - rotate around Y(0, 0, 1)
   float heading = Deg2Rad(cp.orientation.r); // yaw
   float pitch = Deg2Rad(cp.orientation.g);
   float roll = Deg2Rad(cp.orientation.b);
   // heading
   right = normalize(glm::rotate(right, heading, up));
   dir = normalize(glm::rotate(dir, heading, up));
   // pitch
   up = normalize(glm::rotate(up, pitch, right));
   dir = normalize(glm::rotate(dir, pitch, right));
   // roll
   up = normalize(glm::rotate(up, roll, dir));
   right = normalize(glm::rotate(right, roll, dir));
   SetFieldOfView(cp.fovX, cp.fovY);
}