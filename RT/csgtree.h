#pragma once

#include "shape.h"
#include "csgnode.h"

namespace CSG {

class Tree : public Shape
{
public:
   Tree (Node *root, Material *mat) : Shape(mat), pRoot(root) {}
   ~Tree();
   virtual glm::vec3 GetNormal(const Ray &R, float dist);
   virtual RayIntersection FindIntersection(const Ray &R);

   void SetRoot(Node *pNode) { pRoot = pNode; }
private:
   Node *pRoot;
};

}