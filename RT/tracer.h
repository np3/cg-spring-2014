#pragma once
#include "material.h"

class Scene;
class Renderer;
class Ray;
class Camera;
struct RayIntersection;

struct ThreadData
{
  long tileID;
  int resX;
  int resY;
  void* barrier;
  const Scene *scenePtr;
  Renderer *pRenderer;
  int traceDepth;
  Camera *pCam;
};

class RayTracer
{
public:
  RayTracer(const Scene&);
  void Run(Renderer *pRenderer, int traceDepth, int resX, int resY);
  void RunMT(Renderer *pRenderer, int traceDepth, int resX, int resY);
private:
  static Color3 Trace(const Scene& scn, const Ray &primaryRay, float reflectionAmount, int traceDepth);
  static Color3 TraceImpl(const Scene& scn, const Ray &primaryRay, RayIntersection *ri);
  const Scene& scn;
  static unsigned long __stdcall MTRender(void *threadData);
  static const int MAX_THREADS = 256;
  static const int TILE_SIZE = 32;
};