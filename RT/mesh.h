#pragma once
#include "shape.h"
#include "aabb.h"
#include <vector>

class Mesh : public Shape
{
  friend class ObjLoader;
public:
  Mesh(Material *pM) : Shape(pM) {}
  Mesh(const std::vector<Triangle *> _faces, const AABB &_aabb, Material *pM) :
    Shape(pM), faces(_faces), aabb(_aabb) {}
  ~Mesh();
  RayIntersection FindIntersection(const Ray &R);
  glm::vec3 GetNormal(const Ray &R, float dist);
private:
  std::vector<Triangle *> faces;
  AABB aabb;
};