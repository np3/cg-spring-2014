#pragma once
#include <vector>
#include "camera.h"
#include "shape.h"
#include "material.h"
#include <glm/glm.hpp>

class Shape;
class Light;
struct Material;

class Scene
{
  friend class SceneReader;
  friend class Tracer;
public:
  ~Scene();
  void AddShape(Shape *pS);
  void AddLight(Light *pL);
  RayIntersection FindIntersection(const Ray &r, bool any) const;
  Color3 CalcIllumination(const Shape* hitShape, const Ray& primaryRay, 
                          float dist, const glm::vec3 &norm) const;
  const CameraParams& GetCamParams() const { return camParams; }
  void SetCamParams(const CameraParams &cp) { camParams = cp; }
private:
  std::vector<Shape *> objects;
  std::vector<Light *> lights;
  std::vector<Material *> sharedMaterials;
  CameraParams camParams;
  void SetMtlSet(const std::vector<Material *> & mV) { sharedMaterials = mV; }
};