#pragma once
#include "csgnode.h"

namespace CSG {

class Union : public OpNode
{
public:
   Union(Node *l, Node *r) : OpNode(l, r) {}
   RayIntersection FindIntersection(const Ray &R);
   glm::vec3 GetNormal(const Ray &R, float dist);
};

class Intersection : public OpNode
{
public:
   Intersection(Node *l, Node *r) : OpNode(l, r) {}
   RayIntersection FindIntersection(const Ray &R);
   glm::vec3 GetNormal(const Ray &R, float dist);
};

class Difference : public OpNode
{
public:
   Difference(Node *l, Node *r) : OpNode(l, r) {}
   RayIntersection FindIntersection(const Ray &R);
   glm::vec3 GetNormal(const Ray &R, float dist);
};

}