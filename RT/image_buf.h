#ifndef _IMAGE_BUF_H_
#define _IMAGE_BUF_H_
#include "common.h"

class ImageBuf
{
private:
  DWORD *buffer;
  int W, H;
public:
  ImageBuf() : W(0), H(0), buffer(nullptr) {}
  ImageBuf(int nW, int nH) {
    Resize(nW, nH);
  }
  ~ImageBuf() {
    Release();
  }
  void Resize(int nW, int nH) {
    W = nW;
    H = nH;
    buffer = new DWORD[W * H];
  }
  void Release() {
    W = 0;
    H = 0;
    delete[] buffer;
  }
  inline void SetPixel(int x, int y, DWORD val) {
    buffer[y * W + x] = val;
  }
  inline DWORD GetPixel(int x, int y) {
    return buffer[y * W + x];
  }
private:
  ImageBuf(const ImageBuf&);
  ImageBuf& operator=(const ImageBuf&);
};
#endif