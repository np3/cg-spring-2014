#include "csgops.h"

using namespace CSG;
using namespace glm;

vec3 Union::GetNormal(const Ray &R, float dist) { return vec3(0); }
vec3 Intersection::GetNormal(const Ray &R, float dist) { return vec3(0); }
vec3 Difference::GetNormal(const Ray &R, float dist) { return vec3(0); }

RayIntersection Union::FindIntersection(const Ray &R) {
   RayIntersection aMin = pLeft->FindIntersection(R);
   RayIntersection bMin = pRight->FindIntersection(R);

   if (aMin.exists && !bMin.exists) {
      return aMin;
   } else if (!aMin.exists && bMin.exists) {
      return bMin;
   } else {
      return aMin.dist < bMin.dist ? aMin : bMin;
   }
   return RayIntersection(false);
}

RayIntersection Intersection::FindIntersection(const Ray &R) {
   RayIntersection aMin = pLeft->FindIntersection(R);
   RayIntersection bMin = pRight->FindIntersection(R);

   if (aMin.exists && bMin.exists) {
      RayIntersection aFar = GetFarIntersection(aMin, pLeft, R);
      RayIntersection bFar = GetFarIntersection(bMin, pRight, R);

      if (aMin.dist < bMin.dist && aFar.dist > bMin.dist) {
         return bMin;
      } else if (bMin.dist < aMin.dist && bFar.dist > aMin.dist) {
         return aMin;
      }
   }
   return RayIntersection(false);
}

RayIntersection Difference::FindIntersection(const Ray &R) {
   RayIntersection aMin = pLeft->FindIntersection(R);
   RayIntersection bMin = pRight->FindIntersection(R);

   if (!aMin.exists) {
      return RayIntersection(false);
   }
   if (!bMin.exists) {
      return aMin;
   }
   RayIntersection aFar = GetFarIntersection(aMin, pLeft, R);
   RayIntersection bFar = GetFarIntersection(bMin, pRight, R);

   if (bFar.dist < aMin.dist) {
      return aMin;
   }
   if (aFar.dist < bMin.dist) {
      return aMin;
   }
   if (bFar.dist < aFar.dist) {
      bFar.normal *= -1.0f;
      return bFar;
   }
   return RayIntersection(false);
}