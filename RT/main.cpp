#include "common.h"
#include <windows.h>
#include <atlbase.h>
#include <atlwin.h>
#include <atltypes.h>
#include "renderer.h"
#include "scene.h"
#include "scene_reader.h"
#include "tracer.h"
#include <string>
#include "cmdparser.h"
#include <cassert>
#include <iostream>

using namespace std;

class RTOutput : public CWindowImpl<RTOutput, CWindow, CFrameWinTraits>
{
  GLRenderer &renderer;
public:
  RTOutput(GLRenderer &r) : renderer(r) {}
private:
  BEGIN_MSG_MAP(RTOutput)
    MESSAGE_HANDLER(WM_CREATE,  OnCreate)
    MESSAGE_HANDLER(WM_SIZE,    OnSize)
    MESSAGE_HANDLER(WM_TIMER,   OnTimer)
    MESSAGE_HANDLER(WM_PAINT,   OnPaint)
    MESSAGE_HANDLER(WM_KEYDOWN, OnKeyDown)
    MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
  END_MSG_MAP()
  LRESULT OnCreate(UINT msg, WPARAM wParam, LPARAM lParam ,BOOL &bHandled)
  {
    SetTimer(0, 1);
    return 0;
  }
  LRESULT OnSize(UINT msg, WPARAM wParam, LPARAM lParam ,BOOL &bHandled)
  {
    int W = LOWORD(wParam);
    int H = HIWORD(wParam);

    renderer.Resize(W, H);
    return 0;
  }
  LRESULT OnTimer(UINT msg, WPARAM wParam, LPARAM lParam ,BOOL &bHandled)
  {
    renderer.Render();
    InvalidateRect(NULL, FALSE);
    return 0;
  }
  LRESULT OnPaint(UINT msg, WPARAM wParam, LPARAM lParam ,BOOL &bHandled)
  {
    PAINTSTRUCT ps;
    HDC hDC = BeginPaint(&ps);
    renderer.CopyFrame(hDC);
    EndPaint(&ps);
    return 0;
  }
  LRESULT OnKeyDown(UINT msg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
  {
    if (LOWORD(wParam) == VK_ESCAPE) {
      SendMessage(WM_DESTROY);
    }
    return 0;
  }
  LRESULT OnDestroy(UINT msg, WPARAM wParam, LPARAM lParam, BOOL &bHandled)
  {
    KillTimer(0);
    PostQuitMessage(0);
    Detach();
    return 0;
  }
public:
  int Loop() {
    MSG Msg;
    while(1)
    {
      if(PeekMessage(&Msg, NULL, 0, 0, PM_NOREMOVE))
      {
        if(!GetMessage(&Msg, NULL, 0, 0)) {
          break;
        }
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
      }
      else {
        Sleep(1);
      }
    }
    return 0;
  }
};

struct CmdParams
{
  int traceDepth;
  std::string inputName;
  std::string outputName;
  int resX;
  int resY;
  bool wndOutput;

  CmdParams() : traceDepth(0), resX(1024), resY(1024), wndOutput(false) {
    inputName = "scene.yml";
    outputName = "result.bmp";
  }
};

inline bool ParseCmdLine(CmdParams &params, int argc, char **argv)
{
  enum OptionIndex { SCENE, RES_X, RES_Y, OUTPUT, TRACE_DEPTH, WINDOW_OUTPUT, UNKNOWN };
  const option::Descriptor usage[] =
  {
    {UNKNOWN, 0, "", "", option::Arg::None, "USAGE: RT [options]\n\n"
                                            "Options:"},
    {SCENE,   0, "s", "scene", option::Arg::Optional, "  --scene \tScene description filename"},
    {RES_X,   0, "w", "resolution_x", option::Arg::Optional, "  --resolution_x \tViewport width"},
    {RES_Y,   0, "h", "resolution_y", option::Arg::Optional, "  --resolution_y \tViewport height"},
    {OUTPUT,  0, "o", "output", option::Arg::Optional, "  --output \tOutput picture filename"},
    {TRACE_DEPTH,  0, "d", "trace_depth", option::Arg::Optional, "  --trace_depth \tTrace procedure recursion depth"},
    {WINDOW_OUTPUT,  0, "wnd", "window", option::Arg::None, "  --window Show image in a separate window"},
    {0, 0, 0, 0, 0, 0}
  };
  option::Stats stats(usage, argc, argv);
  option::Option *options = new option::Option[stats.options_max];
  option::Option *buffer = new option::Option[stats.buffer_max];
  option::Parser parse(usage, argc, argv, options, buffer);

  if (parse.error()) {
    return false;
  }
  bool bErr = false;
  for (int i = 0; i < parse.optionsCount(); ++i) {
    option::Option& opt = buffer[i];
    const char *arg = opt.arg;
    int id = opt.index();
    if (id != WINDOW_OUTPUT && arg == NULL) {
      bErr = true;
      break;
    }
    switch (id)
    {
      case SCENE:
        params.inputName = arg;
        break;
      case RES_X:
        params.resX = strtol(arg, NULL, 10);
        break;
      case RES_Y:
        params.resY = strtol(arg, NULL, 10);
        break;
      case OUTPUT:
        params.outputName = arg;
        break;
      case TRACE_DEPTH:
        params.traceDepth = strtol(arg, NULL, 10);
        break;
      case WINDOW_OUTPUT:
        params.wndOutput = true;
        break;
      default:
        break;
    }
  }
  delete[] options;
  delete[] buffer;
  return !bErr;
}

int main(int argc, char **argv)
{
  CmdParams params;
  argc -= (argc > 0);
  argv += (argc > 0);
  ParseCmdLine(params, argc, argv);
  SceneReader scnReader;
  Scene scn;
  std::cout << "Scene name: " << params.inputName << std::endl;
  std::cout << "Output picture name: " << params.outputName << std::endl;
  std::cout << "Size X: " << params.resX << std::endl;
  std::cout << "Size Y: " << params.resY << std::endl;
  std::cout << "Trace depth: " << params.traceDepth << std::endl;
  if (scnReader.Read(params.inputName.c_str(), scn)) {
    RayTracer tracer(scn);
    Renderer renderer;
    
    tracer.RunMT(&renderer, params.traceDepth, params.resX, params.resY);
    if (params.wndOutput) {
      renderer.DisplayWait();
      return 0;
    }
    renderer.SaveFrame(params.outputName.c_str());
    return 0;
  }
  return -1;
}
