#include "common.h"
#include "aabb.h"
#include <algorithm>

using namespace glm;
bool AABB::Intersects(const Ray &R)
{
  const vec3 &origin = R.Origin(), &direction = R.Dir();
  float d0 = -INFINITY, d1 = INFINITY;

  if (abs(direction.x) > EPS)
  {
    d0 = (min.x - origin.x) / direction.x;
    d1 = (max.x - origin.x) / direction.x;

    if (d1 < d0) {
      std::swap(d0, d1);
    }
  }

  if (abs(direction.y) > EPS)
  {
    float t0, t1;
    t0 = (min.y - origin.y) / direction.y;
    t1 = (max.y - origin.y) / direction.y;

    if (t1 < t0) {
      std::swap(t0, t1);
    }

    d0 = std::max(d0,t0);
    d1 = std::min(d1,t1);
  }

  if (abs(direction.z) > EPS)
  {
    float t0, t1;
    t0 = (min.z - origin.z) / direction.z;
    t1 = (max.z - origin.z) / direction.z;

    if (t1 < t0) {
      std::swap(t0, t1);
    }

    d0 = std::max(d0,t0);
    d1 = std::min(d1,t1);
  }

  // d1 < d0 || d0 == -INFINITY
  if (d1 < d0 || abs(d0 - INFINITY) < EPS) {
    return false;
  }

  return true;
}