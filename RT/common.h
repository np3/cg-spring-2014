#pragma once

#define PI 3.14159265358979f
#define INFINITY    1e6f
#define EPS         1e-5f
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef unsigned int  UINT;
#define Deg2Rad(x) ((x) * PI / 180.0f)
