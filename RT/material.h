#pragma once

#include <glm/glm.hpp>

typedef glm::vec3 Color3;

struct ColorDesc
{
  Color3    ambient;   // ambient lighting
  Color3    diffuse;   // diffuse lighting
  Color3    specular;  // specular lighting
  Color3    emissive;  // emissive lighting
  float     specPower; // amount of specular lighting / specular power

  ColorDesc()
    : ambient(Color3(0.0f, 0.0f, 0.0f)),
      diffuse(Color3(1.0f, 0.0f, 0.0f)),
      specular(Color3(1.0f, 0.0f, 0.0f)),
      emissive(Color3(0.0f, 0.0f, 0.0f)),
      specPower(0)
  { }
  ColorDesc(const Color3 &ambient, const Color3 &diffuse, const Color3 &specular,
    float shininess = 0, const Color3 &emissive = Color3())
    : ambient(ambient), diffuse(diffuse), specular(specular),
      emissive(emissive), specPower(shininess)
    {}
};

struct Material
{
  Material() : reflection(0.f) {}
  ColorDesc color;

  float	reflection;

  const Color3& ambient() const { return color.ambient; }
  const Color3& diffuse() const { return color.diffuse; }
  const Color3& specular() const { return color.specular; }
  float specPower() const { return color.specPower; }
};