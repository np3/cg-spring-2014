#pragma once
#include <vector>

class Scene;
namespace YAML {
  class Node;
}
namespace CSG {
  class Node;
}
class Shape;
struct Material;

class SceneReader
{
public:
  bool Read(const char *pFileName, Scene &scn);
private:
  // !!! DON'T CHANGE ORDER
  enum NODE_TYPE {
    NODE_NONE = -1,
    NODE_SPHERE,
    NODE_PLANE,
    NODE_CYL,
    NODE_CONE,
    NODE_TORUS,
    NODE_OBJ,
    NODE_CSG_DIFF,
    NODE_CSG_UNITE,
    NODE_CSG_ISECT
  };
  struct SCENE_RECORD {
    int matID;
    NODE_TYPE type;
    const YAML::Node *selNode;
  };
  inline CSG::Node* csgNodeParse(const YAML::Node* node, struct ShapeTransform *prevTransform = nullptr);
  Shape* nodeParse(const YAML::Node* node, Scene &scn, struct ShapeTransform *prevTransform = nullptr);
  std::vector<Material*> materials;
  Shape* createSceneEntity(const SCENE_RECORD &rec, struct ShapeTransform &transform);
  inline Shape* createSphere(const YAML::Node* node);
  inline Shape* createPlane(const YAML::Node* node);
  inline Shape* createCyl(const YAML::Node* node);
  inline Shape* createCone(const YAML::Node* node);
  inline Shape* createTorus(const YAML::Node* node);
  inline Shape* createObj(const YAML::Node* node);
  inline Shape* createCSGTree(NODE_TYPE t, const YAML::Node* node, struct ShapeTransform *prevTransform = nullptr);
  inline CSG::Node* createCSGTreeImpl(NODE_TYPE t, const YAML::Node* node, struct ShapeTransform *prevTransform = nullptr);
  std::vector<int> mtlIndices;
};