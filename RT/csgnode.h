#pragma once

#include "shape.h"

namespace CSG {

class Node : public Shape
{
public:
   Node();
   virtual ~Node();
};

class LeafNode : public Node
{
public:
   LeafNode(Shape *pShape) : nodeShape(pShape) {}
   ~LeafNode();
   RayIntersection FindIntersection(const Ray &r);
   glm::vec3 GetNormal(const Ray &r, float dist);
private:
   Shape *nodeShape;
};

class OpNode : public Node
{
public:
   OpNode(Node *l, Node *r) : pLeft(l), pRight(r) {}
   ~OpNode();
protected:
   RayIntersection GetFarIntersection(const RayIntersection &nearIsect, Node *pNode, const Ray &r) const;
   Node *pLeft, *pRight;
};

}
