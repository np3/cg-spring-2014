#include <fstream>
#include "scene_reader.h"
#include <yaml-cpp/yaml.h>
#include <yaml-cpp/aliasmanager.h>
#include <yaml-cpp/eventhandler.h>
#include <iostream>
#include "camera.h"
#include "material.h"
#include "shape.h"
#include "scene.h"
#include "objloader.h"
#include "light.h"
#include "csgtree.h"
#include "csgops.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

void parseLCS(const YAML::Node &node, ShapeTransform &tr)
{
  glm::vec3 translation(0);
  glm::vec3 orientation(0);
  glm::vec3 scale(1.0f, 1.0f, 1.0f);

  if (auto rec = node.FindValue("x")) {
    *rec >> translation.x;
  }
  if (auto rec = node.FindValue("y")) {
    *rec >> translation.z;
  }
  if (auto rec = node.FindValue("z")) {
    *rec >> translation.y;
  }
  if (auto rec = node.FindValue("h")) {
    *rec >> orientation.x;
  }
  if (auto rec = node.FindValue("p")) {
    *rec >> orientation.y;
  }
  if (auto rec = node.FindValue("r")) {
    *rec >> orientation.z;
  }
  if (auto rec = node.FindValue("sx")) {
    *rec >> scale.x;
  }
  if (auto rec = node.FindValue("sy")) {
    *rec >> scale.z;
  }
  if (auto rec = node.FindValue("sz")) {
    *rec >> scale.y;
  }
  // Translate
  tr.transformMatrix = glm::translate(tr.transformMatrix, translation);
  // Rotation
  tr.transformMatrix *= glm::yawPitchRoll(Deg2Rad(-orientation.x), Deg2Rad(-orientation.y), Deg2Rad(-orientation.z));
  // Scale
  tr.transformMatrix = glm::scale(tr.transformMatrix, scale);
}

void parseColor(const YAML::Node &node, glm::vec3 &vec) {
  memset(&vec, 0, sizeof(glm::vec3));
  if (auto val = node.FindValue("r")) {
    *val >> vec.x;
  }
  if (auto val = node.FindValue("g")) {
    *val >> vec.y;
  }
  if (auto val = node.FindValue("b")) {
    *val >> vec.z;
  }
}

void parseVec(const YAML::Node &node, glm::vec3 &vec)
{
  memset(&vec, 0, sizeof(glm::vec3));
  if (auto val = node.FindValue("x")) {
    *val >> vec.x;
  }
  if (auto val = node.FindValue("y")) {
    *val >> vec.y;
  }
  if (auto val = node.FindValue("z")) {
    *val >> vec.z;
  }
}

void parseOrientation(const YAML::Node &node, glm::vec3 &vec)
{
  memset(&vec, 0, sizeof(glm::vec3));
  if (auto val = node.FindValue("h")) {
    *val >> vec.x;
    vec.x = -vec.x;
  }
  if (auto val = node.FindValue("p")) {
    *val >> vec.y;
    vec.y = -vec.y;
  }
  if (auto val = node.FindValue("r")) {
    *val >> vec.z;
    vec.z = -vec.z;
  }
}

void parseXYZ(const YAML::Node &node, glm::vec3 &vec)
{
  parseVec(node, vec);
  std::swap(vec.y, vec.z);
}

void operator>>(const YAML::Node &node, CameraParams &cam)
{
  if (auto pos = node.FindValue("position")) {
     parseXYZ(*pos, cam.pos);//*pos >> cam.pos;
  }
  if (auto or = node.FindValue("orientation")) {
    parseOrientation(*or, cam.orientation);
  }
  if (auto fX = node.FindValue("fov_x")) {
    *fX >> cam.fovX;
  }
  if (auto fY = node.FindValue("fov_y")) {
    *fY >> cam.fovY;
  }
}

void operator>>(const YAML::Node &node, ColorDesc &cd)
{
  if (auto ambient = node.FindValue("ambient")) {
    parseColor(*ambient, cd.ambient);
  }
  if (auto diffuse = node.FindValue("diffuse")) {
    parseColor(*diffuse, cd.diffuse);
  }
  if (auto specular = node.FindValue("specular")) {
    parseColor(*specular, cd.specular);
  }
  if (auto specPwr = node.FindValue("specular_power")) {
    *specPwr >> cd.specPower;
  }
}

void operator>>(const YAML::Node &node, Material &cd)
{
  node >> cd.color;
  if (auto reflFactor = node.FindValue("reflection_factor")) {
    *reflFactor >> cd.reflection;
  }
}

void operator>>(const YAML::Node &node, Light &l)
{
   if (auto rec = node.FindValue("direction")) {
     parseXYZ(*rec, l.dir);//*rec >> l.dir;
   }
   if (auto rec = node.FindValue("position")) {
     parseXYZ(*rec, l.pos);//*rec >> l.pos;
   }
   if (auto rec = node.FindValue("color")) {
     parseColor(*rec, l.color.ambient);
     memcpy_s(&l.color.diffuse, sizeof(Color3), &l.color.ambient, sizeof(Color3));
     memcpy_s(&l.color.specular, sizeof(Color3), &l.color.ambient, sizeof(Color3));
   }
   if (auto rec = node.FindValue("distance")) {
     *rec >> l.range;
   }
   if (auto rec = node.FindValue("fade_exponent")) {
     *rec >> l.fadeExp;
   }
   if (auto rec = node.FindValue("specular_model")) {
     std::string type;
     *rec >> type;
     if (type == "phong") {
        l.specModel = LIGHT_SPEC_PHONG;
     } else if (type == "blinn_phong") {
        l.specModel = LIGHT_SPEC_BLINN_PHONG;
     }
   }
   l.dir = glm::normalize(l.dir);
}

struct AliasSpecialHandler : public YAML::EventHandler
{
  std::string name;
  unsigned int anchorId;

  typedef YAML::Mark Mark;
  typedef YAML::anchor_t anchor_t;
  int lineAnchor;
  void OnDocumentStart(const Mark& mark) {}
  void OnDocumentEnd() {}

  void OnNull(const Mark& mark, anchor_t anchor) {}
  void OnAlias(const Mark& mark, anchor_t anchor) {}
  void OnScalar(const Mark& _mark, const std::string& tag, anchor_t anchor, const std::string& value) {
    lineAnchor = _mark.line;
    name = value;
  }
  void OnSequenceStart(const Mark& mark, const std::string& tag, anchor_t anchor) {}
  void OnSequenceEnd() {}
  void OnMapStart(const Mark& mark, const std::string& tag, anchor_t anchor) {}
  void OnMapEnd() {}
};

inline void accumTransform(ShapeTransform &accum, const ShapeTransform &cur) {
   accum.transformMatrix *= cur.transformMatrix;
}

CSG::Node* SceneReader::csgNodeParse(const YAML::Node* node, ShapeTransform *prevTransform) {
  const auto& nodeInst = *node;
  SCENE_RECORD rec;
  rec.matID = -1;
  rec.selNode = NULL;
  rec.type = NODE_NONE;
  ShapeTransform currentTransform;

  if (prevTransform) {
    accumTransform(currentTransform, *prevTransform);
  }

  for (int i = 0; i < (int)nodeInst.size(); ++i) {
    const auto& curNode = nodeInst[i];
    if (nodeInst[i].IsAliased()) {
      AliasSpecialHandler ee;
      // check if material
      nodeInst[i].EmitEvents(ee);
      int mtlID = -1;
      for (UINT i = 0; i < mtlIndices.size(); ++i) {
        if (mtlIndices[i] == ee.lineAnchor) {
          mtlID = i;
          break;
        }
      }
      if (ee.name == "material") {
        rec.matID = mtlID;
      }
    }
    else if (auto sphere = nodeInst[i].FindValue("sphere")) {
      rec.selNode = (YAML::Node *)sphere;
      rec.type = NODE_SPHERE;
    }
    else if (auto plane = nodeInst[i].FindValue("plane")) {
      rec.selNode = (YAML::Node *)plane;
      rec.type = NODE_PLANE;
    }
    else if (auto cyl = nodeInst[i].FindValue("cylinder")) {
      rec.selNode = (YAML::Node *)cyl;
      rec.type = NODE_CYL;
    }
    else if (auto cone = nodeInst[i].FindValue("cone")) {
      rec.selNode = (YAML::Node *)cone;
      rec.type = NODE_CONE;
    }
    else if (auto torus = nodeInst[i].FindValue("torus")) {
      rec.selNode = (YAML::Node *)torus;
      rec.type = NODE_TORUS;
    }
    else if (auto obj = nodeInst[i].FindValue("obj_model")) {
      rec.selNode = (YAML::Node *)obj;
      rec.type = NODE_OBJ;
    }
    else if (auto csg = nodeInst[i].FindValue("csg_difference")) {
      return createCSGTreeImpl(NODE_CSG_DIFF, csg, prevTransform);
    }
    else if (auto csg = nodeInst[i].FindValue("csg_union")) {
      return createCSGTreeImpl(NODE_CSG_UNITE, csg, prevTransform);
    }
    else if (auto csg = nodeInst[i].FindValue("csg_intersection")) {
      return createCSGTreeImpl(NODE_CSG_ISECT, csg, prevTransform);
    }
  }
  if (rec.selNode != NULL && rec.matID != -1 && rec.type != NODE_NONE) {
    auto shape = createSceneEntity(rec, currentTransform);
    return new CSG::LeafNode(shape);
  }
  return NULL;
}

Shape* SceneReader::nodeParse(const YAML::Node* node, Scene &scn, ShapeTransform *prevTransform) {
  const auto& nodeInst = *node;
  SCENE_RECORD rec;
  rec.matID = -1;
  rec.selNode = NULL;
  rec.type = NODE_NONE;
  ShapeTransform currentTransform;

  if (prevTransform) {
    accumTransform(currentTransform, *prevTransform);
  }

  for (int i = 0; i < (int)nodeInst.size(); ++i) {
    const auto& curNode = nodeInst[i];
    if (nodeInst[i].IsAliased()) {
        AliasSpecialHandler ee;
        // check if material
        nodeInst[i].EmitEvents(ee);
        int mtlID = -1;
        for (UINT i = 0; i < mtlIndices.size(); ++i) {
          if (mtlIndices[i] == ee.lineAnchor) {
            mtlID = i;
            break;
          }
        }
        if (ee.name == "material") {
          rec.matID = mtlID;
        }
    }
    else if (auto lcs = nodeInst[i].FindValue("lcs")) {
      parseLCS(*lcs, currentTransform);
    }
    else if (auto newNode = nodeInst[i].FindValue("node")) {
      nodeParse(newNode, scn, &currentTransform);
    }
    else if (auto sphere = nodeInst[i].FindValue("sphere")) {
       rec.selNode = (YAML::Node *)sphere;
       rec.type = NODE_SPHERE;
    }
    else if (auto plane = nodeInst[i].FindValue("plane")) {
      rec.selNode = (YAML::Node *)plane;
      rec.type = NODE_PLANE;
    }
    else if (auto cyl = nodeInst[i].FindValue("cylinder")) {
      rec.selNode = (YAML::Node *)cyl;
      rec.type = NODE_CYL;
    }
    else if (auto cone = nodeInst[i].FindValue("cone")) {
      rec.selNode = (YAML::Node *)cone;
      rec.type = NODE_CONE;
    }
    else if (auto torus = nodeInst[i].FindValue("torus")) {
      rec.selNode = (YAML::Node *)torus;
      rec.type = NODE_TORUS;
    }
    else if (auto obj = nodeInst[i].FindValue("obj_model")) {
      rec.selNode = (YAML::Node *)obj;
      rec.type = NODE_OBJ;
    }
    else if (auto csg = nodeInst[i].FindValue("csg_difference")) {
      rec.type = NODE_CSG_DIFF;
      rec.selNode = csg;
    }
    else if (auto csg = nodeInst[i].FindValue("csg_union")) {
      rec.type = NODE_CSG_UNITE;
      rec.selNode = csg;
    }
    else if (auto csg = nodeInst[i].FindValue("csg_difference")) {
      rec.type = NODE_CSG_ISECT;
      rec.selNode = csg;
    }
  }
  if (rec.selNode != NULL && rec.type != NODE_NONE) {
     auto shape = createSceneEntity(rec, currentTransform);
     scn.AddShape(shape);
     return shape;
  }
  return NULL;
}

Shape* SceneReader::createSphere(const YAML::Node* node)
{
  float rad = 1.0f;
  glm::vec3 center(0);
  assert(node->size() == 2);
  if (auto rec = node->FindValue("radius")) {
    *rec >> rad;
  }
  if (auto rec = node->FindValue("center")) {
    parseXYZ(*rec, center);//*rec >> center;
  }
  return new Sphere(center, rad, NULL);
}

Shape* SceneReader::createPlane(const YAML::Node* node)
{
  glm::vec3 n(0, 1, 0);
  float d = 0.0f;
  assert(node->size() == 2);
  if (auto rec = node->FindValue("normal")) {
    parseVec(*rec, n);
  }
  if (auto rec = node->FindValue("d")) {
    *rec >> d;
  }
  return new Plane(n, d, NULL);
}

Shape* SceneReader::createCyl(const YAML::Node* node)
{
  glm::vec3 botC(0);
  glm::vec3 topC(0, 1, 0);
  float rad = 1.0f;
  assert(node->size() == 2);
  if (auto rec = node->FindValue("radius")) {
    *rec >> rad;
  }
  if (auto rec = node->FindValue("height")) {
    *rec >> topC.y;
  }
  return new Cylinder(botC, topC, rad, NULL);
}

Shape* SceneReader::createCone(const YAML::Node* node)
{
  glm::vec3 botC(0);
  glm::vec3 topC(0, 1, 0);
  float rad = 1.0f;
  assert(node->size() == 2);
  if (auto rec = node->FindValue("radius")) {
    *rec >> rad;
  }
  if (auto rec = node->FindValue("height")) {
    *rec >> topC.y;
  }
  return new Cone(botC, topC, rad, NULL);
}

Shape* SceneReader::createTorus(const YAML::Node* node) {
  float innerRad = 1.0f;
  float outerRad = 2.0f;
  assert(node->size() == 2);
  if (auto rec = node->FindValue("innerRadius")) {
    *rec >> innerRad;
  }
  if (auto rec = node->FindValue("outerRadius")) {
    *rec >> outerRad;
  }
  return new Torus(innerRad, outerRad, glm::vec3(0), NULL);
}

Shape* SceneReader::createObj(const YAML::Node* node) {
  assert(node->size() == 1);
  std::string fileName;

  if (auto rec = node->FindValue("file_name")) {
    *rec >> fileName;
  }
  ObjLoader loader;
  return (Shape *)loader.createMesh(fileName.c_str());
}

CSG::Node* SceneReader::createCSGTreeImpl(NODE_TYPE type, const YAML::Node* node, ShapeTransform *prevTransform) {
  CSG::Node *leftNode = NULL, *rightNode = NULL;
  if (auto left = node->FindValue("left_node")) {
     leftNode = csgNodeParse(left, prevTransform);
  }
  if (auto right = node->FindValue("right_node")) {
     rightNode = csgNodeParse(right, prevTransform);
  }
  assert(leftNode && rightNode);
  CSG::OpNode *opNode = NULL;
  switch (type)
  {
    case SceneReader::NODE_CSG_DIFF:
      opNode = new CSG::Difference(leftNode, rightNode);
      break;
    case SceneReader::NODE_CSG_UNITE:
      opNode = new CSG::Union(leftNode, rightNode);
      break;
    case SceneReader::NODE_CSG_ISECT:
      opNode = new CSG::Intersection(leftNode, rightNode);
      break;
    default:
      assert(!"Wrong CSG type");
      break;
  }
  return opNode;
}

Shape* SceneReader::createCSGTree(NODE_TYPE t, const YAML::Node* node, struct ShapeTransform *prevTransform) {
   CSG::Tree *tree = new CSG::Tree(NULL, NULL);
   CSG::Node *root = createCSGTreeImpl(t, node, prevTransform);
   assert(root);
   tree->SetRoot(root);

   return tree;
}

Shape* SceneReader::createSceneEntity(const SCENE_RECORD &rec, ShapeTransform &transform) {
  if (rec.type < NODE_CSG_DIFF) {
    assert(rec.matID >= 0);
  }
  assert(rec.selNode != NULL);
  assert(rec.type != NODE_NONE);

  Shape *pNewShape = NULL;
  switch (rec.type) {
    case NODE_SPHERE:
      pNewShape = createSphere(rec.selNode);
      break;
    case NODE_PLANE:
      pNewShape = createPlane(rec.selNode);
      break;
    case NODE_CYL:
      pNewShape = createCyl(rec.selNode);
      break;
    case NODE_CONE:
      pNewShape = createCone(rec.selNode);
      break;
    case NODE_TORUS:
      pNewShape = createTorus(rec.selNode);
      break;
    case NODE_OBJ:
      pNewShape = createObj(rec.selNode);
      break;
    case NODE_CSG_DIFF:
    case NODE_CSG_UNITE:
    case NODE_CSG_ISECT:
      pNewShape = createCSGTree(rec.type, rec.selNode, &transform);
      break;
  }
  if (pNewShape) {
     if (rec.type < NODE_CSG_DIFF) {
       pNewShape->SetMaterial(materials[rec.matID]);
     }
     pNewShape->ApplyTransform(transform);
     return pNewShape;
  }
  return NULL;
}

bool SceneReader::Read(const char *pFileName, Scene &scn)
{
  std::ifstream input(pFileName);
  if (!input.is_open()) {
    return false;
  }
  try {
    YAML::Parser parser(input);
    YAML::Node doc;

    parser.GetNextDocument(doc);
    for (int i = 0; i < (int)doc.size(); ++i) {
      if (auto camNode = doc[i].FindValue("camera")) {
        CameraParams camParams;
        *camNode >> camParams;
        scn.SetCamParams(camParams);
      }
      else if (auto matNode = doc[i].FindValue("material")) {
        materials.push_back(new Material());
        *matNode >> *materials.back();
        auto mark = matNode->GetMark();
        mtlIndices.push_back(mark.line - 1);
      }
      else if (auto node = doc[i].FindValue("node")) {
        nodeParse(node, scn);
      }
      else if (auto dirLightNode = doc[i].FindValue("directional_light")) {
        Light *pNewLight = new Light(LIGHT_DIR);
        *dirLightNode >> *pNewLight;
        scn.AddLight(pNewLight);
      }
      else if (auto pointLightNode = doc[i].FindValue("point_light")) {
        Light *pNewLight = new Light(LIGHT_POINT);
        *pointLightNode >> *pNewLight;
        scn.AddLight(pNewLight);
      }
    }
  }
  catch(YAML::ParserException &e) {
    std::cout << "Error: " << e.msg << std::endl;
    std::cout << "Line: " << e.mark.line << " Column: " << e.mark.column << " Pos: " << e.mark.pos << std::endl;
    return false;
  }
  scn.SetMtlSet(materials); // share materials
  return true;
}