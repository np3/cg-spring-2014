//Petrov Nicolay
#include "config.h"
#include <fstream>
#include <string>
#include <map>
#include <utility>
using namespace std;

typedef multimap<char, string> ParamSet;

Config::Config(const string &Str)
{
  FileName = Str;
  IsLoaded = false;
  Variables = 0;
}

bool Process(ParamSet &M, const string &S)
{
  switch(S[0])
  {
    case 'S':
    case 'C':
    case 'L':
    case 'P':
      M.insert(pair<char, string>(S[0], S));
      return true;
    default:
      return false;

  }
}

void Config::Load()
{
  fstream InputFile;
  string temp;
  string S;
  IsLoaded = true;
  Variables = new ParamSet();
  InputFile.open(FileName.c_str(), ios::in);
  
  if(InputFile.is_open() == false)
  {
    delete static_cast<ParamSet *>(Variables);
    Variables = 0;
    IsLoaded = false;
    return;
  }
  while(getline(InputFile, temp, '\n'))
  {
    S.erase(S.begin(), S.end());
    temp = temp.substr(0, temp.find("#"));
    for(string::iterator i = temp.begin(); i != temp.end(); i++)
      switch(*i)
      {
      
        case '\r':
        case '\t':
        case '\n':
          break;
        default:
          S.append(1, *i);

      }
    if(!S.empty())
      if(Process(*(static_cast<ParamSet *>(Variables)), S) == false)
      {
        delete static_cast<ParamSet *>(Variables);
        IsLoaded = false;
        break;
      }
         
  }
 
  InputFile.close();
}

Config::~Config()
{
  if(Variables != NULL)
    delete static_cast<ParamSet *>(Variables);
}

void* Config::GetVariables() const
{
  return Variables;
}
