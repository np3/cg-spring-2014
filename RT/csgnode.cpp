#include "csgnode.h"

using namespace CSG;

Node::Node()
{
}

Node::~Node()
{
}

OpNode::~OpNode()
{
}

RayIntersection OpNode::GetFarIntersection(const RayIntersection &nearIsect, Node *pNode, const Ray &r) const 
{
   RayIntersection farIsect = nearIsect;
   RayIntersection far(false);
   float dist = nearIsect.dist;

   Ray newRay = Ray(r.Apply(farIsect.dist) + r.Dir(), r.Dir());
   farIsect = pNode->FindIntersection(newRay);
   if (farIsect.exists) {
      far = farIsect;
      dist += farIsect.dist;
   }
   if (far.exists) {
      far.dist = dist;
   } else {
      far.dist = INFINITY;
   }
   return far;
}

LeafNode::~LeafNode() {}
RayIntersection LeafNode::FindIntersection(const Ray &r) {
   return nodeShape->FindIntersection(r);
}

glm::vec3 LeafNode::GetNormal(const Ray &r, float dist) {
   return glm::vec3(0);
}
