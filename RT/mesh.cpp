#include "mesh.h"
#include <cassert>

Mesh::~Mesh() {
  for (int i = 0; i < (int)faces.size(); ++i) {
    delete faces[i];
  }
}

RayIntersection Mesh::FindIntersection(const Ray &_R) {
  Ray R = _R.ApplyMat(InvTransform());
  // first, check Ray vs AABB intersection 
  if (!aabb.Intersects(R)) {
    return RayIntersection(false);
  }
  float closestDist = INFINITY;
  RayIntersection closestInter(false);

  for (int i = 0; i < (int)faces.size(); ++i) {
     RayIntersection cur = faces[i]->FindIntersection(R);
     if (cur.exists) {
       if (cur.dist < closestDist) {
         closestDist = cur.dist;
         closestInter.exists = true;
         closestInter.hitShape = this;
         memcpy_s(&closestInter.normal, sizeof(glm::vec3), &cur.normal, sizeof(glm::vec3));
         memcpy_s(&closestInter.localNormal, sizeof(glm::vec3), &cur.normal, sizeof(glm::vec3));
       }
     }
  }
  if (closestInter.exists) {
    TransformNormal(closestInter.normal);
  }
  return closestInter;
}

glm::vec3 Mesh::GetNormal(const Ray &R, float dist)
{
  assert(!"This method should not be called");
  return glm::vec3(0);
}


