#ifndef _COLOR_H_
#define _COLOR_H_
#include "common.h"

template<typename T>
class Color
{
private:
  T R, G, B, A;
public:
  Color() : R(0), G(0), B(0), A(0) {}
  Color(const T &_R, const T &_G, const T &_B, const T &_A) : R(_R), G(_G), B(_B), A(_A) {}
  Color(DWORD v) { Extract(v); }
  void Extract(DWORD val) {
    R = (val >> 24) & 0xFF;
    G = (val >> 16) & 0xFF;
    B = (val >> 8) & 0xFF;
    A = val & 0xFF;
  }
  DWORD Pack() {
    Clamp_0_255();
    return ((int)R << 24) | ((int)G << 16) | ((int)B << 8) | (int)A;
  }
  
  Color& Clamp_0_1() {
     R = common::Clamp(R, (T)0, (T)1);
     G = common::Clamp(G, (T)0, (T)1);
     B = common::Clamp(B, (T)0, (T)1);
     A = common::Clamp(A, (T)0, (T)1);
     return *this;
  }
  Color& Clamp_0_255() {
    R = common::Clamp(R, (T)0, (T)255);
    G = common::Clamp(G, (T)0, (T)255);
    B = common::Clamp(B, (T)0, (T)255);
    A = common::Clamp(A, (T)0, (T)255);
    return *this;
  }
  Color& Scale_0_1() {
    double maxC = std::max(std::max(R, G), std::max(B, A)) + common::EPS;
    R /= maxC;
    G /= maxC;
    B /= maxC;
    A /= maxC;
    return *this;
  }
  Color& Scale_0_255() {
    R *= 255.0;
    G *= 255.0;
    B *= 255.0;
    A *= 255.0;
    return *this;
  }
  Color& operator+=(const Color&rhs) {
    R += rhs.R;
    G += rhs.G;
    B += rhs.B;
    A += rhs.A;
    return *this;
  }
  Color operator+(const Color& rhs) {
    return Color(R + rhs.R, G + rhs.G, B + rhs.B, A + rhs.A);
  }

  Color operator*(double C) {
    return Color(R * C, G * C, B * C, A * C);
  }
  Color& operator*=(double C) {
    R *= C;
    G *= C;
    B *= C;
    A *= C;
    return *this;
  }
};
#endif