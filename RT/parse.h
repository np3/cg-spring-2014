#ifndef _PARSE_H_
#define _PARSE_H_

#if 0
#include <vector>
#include <map>
#include <utility>
#include <string>
#include "config.h"
#include "shape.h"
#include "camera.h"

std::vector<Shape *> GetObjects(const Config& C);

std::vector<Vec> GetLights(const Config& C);

Camera GetCamera(const Config& C);
#endif

#endif