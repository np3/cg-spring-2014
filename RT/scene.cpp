#include "scene.h"
#include "common.h"
#include "light.h"

Scene::~Scene() {
  for (unsigned int i = 0; i < objects.size(); ++i) {
    delete objects[i];
  }
  for (unsigned int i = 0; i < lights.size(); ++i) {
    delete lights[i];
  }
  for (unsigned int i = 0; i < sharedMaterials.size(); ++i) {
    delete sharedMaterials[i];
  }
}

void Scene::AddLight(Light *pL) {
  lights.push_back(pL);
}

void Scene::AddShape(Shape *pS) {
  objects.push_back(pS);
}

RayIntersection Scene::FindIntersection(const Ray &r, bool any) const {
  float nearestDist = INFINITY;
  RayIntersection nearestHit(false);

  for (unsigned int i = 0; i < objects.size(); ++i)
  {
    RayIntersection hit = objects[i]->FindIntersection(r);
    if (hit.exists && nearestDist > hit.dist) {
      nearestDist = hit.dist;
      nearestHit = hit;
      if (any) {
        break;
      }
    }
  }
  return nearestHit;
}

Color3 Scene::CalcIllumination(const Shape *hitShape, const Ray& primaryRay,
                               float dist, const glm::vec3 &norm) const
{
  Color3 result = (Color3)0;
  for (unsigned int i = 0; i < lights.size(); ++i) {
    result += lights[i]->calcIllum(hitShape, *this, primaryRay, dist, norm);
  }
  return result;
}