#pragma once

#include "material.h"
#include "ray.h"
#include "common.h"
#include <cmath>
#include <glm/mat4x4.hpp>

class Shape;
struct ShapeTransform {
  glm::mat4x4 transformMatrix;

  ShapeTransform() {}
};
struct RayIntersection
{
  bool exists;
  float dist;
  class Shape* hitShape;
  glm::vec3 normal;
  glm::vec3 localNormal;

  RayIntersection(bool bExists) : exists(bExists), dist(-1.0f), hitShape(nullptr) {}
};

class Shape
{
private:
  Material *pMaterial;
  glm::mat4x4 inverseTransformTransposed;
  glm::mat4x4 inverseTransform;
public:
  Shape() : pMaterial(nullptr) {}
  Shape(Material *pM) : pMaterial(pM) {}
  virtual ~Shape() {
  }
  virtual glm::vec3 GetNormal(const Ray &R, float dist) = 0;
  virtual RayIntersection FindIntersection(const Ray &R) = 0;
  void SetMaterial(Material *pM) {
    pMaterial = pM;
  }
  const Material* GetMaterial() const { return pMaterial; }
  void ApplyTransform(const ShapeTransform& tr);
  const glm::mat4x4& InvTransform(void) const { return inverseTransform; }
  const glm::mat4x4& InvTransformTranspose(void) const { return inverseTransformTransposed; }
  void TransformNormal(glm::vec3 &n)
  {
    glm::vec4 _n;
    vec3to4(_n, n);
    _n = InvTransformTranspose() * _n;
    vec4to3(n, _n);
  }
};

class Sphere : public Shape
{
  friend class SceneReader;
private:
  glm::vec3 C;
  float Rad;
public:
  Sphere(const glm::vec3 &nC, float nR, Material *pM) : Shape(pM), C(nC), Rad(nR) {}
  RayIntersection FindIntersection(const Ray &R);
  glm::vec3 GetNormal(const Ray &R, float dist);
};

class Plane : public Shape
{
  friend class SceneReader;
private:
  glm::vec3 N;
  float D;
public:
  Plane(const glm::vec3 &nN, float nD, Material *pM) : Shape(pM), N(nN), D(nD) {}
  RayIntersection FindIntersection(const Ray &R);
  glm::vec3 GetNormal(const Ray &R, float dist);
};

class Triangle : public Shape
{
  friend class SceneReader;
protected:
  glm::vec3 vertA, vertB, vertC;
  glm::vec3 N;
  float S, T;
public:
  Triangle(const glm::vec3 &a, const glm::vec3 &b, 
           const glm::vec3 &c, Material *pM) : Shape(pM), vertA(a), vertB(b), vertC(c) {
    N = glm::normalize(glm::cross(vertB - vertA, vertC - vertA));
  }
  virtual glm::vec3 GetNormal(const Ray&R, float dist);
  virtual RayIntersection FindIntersection(const Ray &R);
};

class TriangleSmooth : public Triangle
{
  friend class SceneReader;
public:
  TriangleSmooth(const glm::vec3 &a, const glm::vec3 &b, const glm::vec3 &c,
                 const glm::vec3 &na, const glm::vec3 &nb, const glm::vec3 &nc,
                 Material *pM) : Triangle(a, b, c, pM), normA(na), normB(nb), normC(nc) {}
  RayIntersection FindIntersection(const Ray &R);
private:
  glm::vec3 normA, normB, normC;
};


class Cylinder : public Shape
{
  friend class SceneReader;
public:
  Cylinder(const glm::vec3 &bC, const glm::vec3 &tC, float r, Material *pM) :
          Shape(pM), bottomCenter(bC), topCenter(tC), radius(r),
          axis(glm::normalize(tC - bC)) {}
  virtual glm::vec3 GetNormal(const Ray &R, float dist);
  virtual RayIntersection FindIntersection(const Ray &R);
private:
  glm::vec3 bottomCenter, topCenter;
  float radius;
  glm::vec3 axis;
  inline glm::vec3 toBottom(const glm::vec3 &v) {
    return v - bottomCenter;
  }
  inline glm::vec3 toTop(const glm::vec3 &v) {
    return v - topCenter;
  }
};

class Cone : public Shape
{
  friend class SceneReader;
public:
  Cone(const glm::vec3 &bC, const glm::vec3 &tC, float rad, Material *pM) :
      Shape(pM), bottomCenter(bC), topCenter(tC), radius(rad) {
    axis = glm::normalize(topCenter - bottomCenter);
    radiusDecrease = radius / (glm::length(topCenter - bottomCenter) + EPS);
  }
  virtual glm::vec3 GetNormal(const Ray &R, float dist);
  virtual RayIntersection FindIntersection(const Ray &R);
private:
  glm::vec3 bottomCenter, topCenter;
  float radius;
  glm::vec3 axis;
  float radiusDecrease;
  inline glm::vec3 toBottom(const glm::vec3 &v) {
    return v - bottomCenter;
  }
  inline glm::vec3 toTop(const glm::vec3 &v) {
    return v - topCenter;
  }
};

class Torus : public Shape
{
  friend class SceneReader;
public:
  Torus(float iR, float oR, const glm::vec3 &p, Material *pM) :
    Shape(pM), innerRad(iR), outerRad(oR), pos(p) {}
  virtual glm::vec3 GetNormal(const Ray &R, float dist);
  virtual RayIntersection FindIntersection(const Ray &R);
private:
  float innerRad, outerRad;
  glm::vec3 pos;
};