#pragma once
#include "common.h"
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <cimg/CImg.h>

class Renderer
{
public:
  Renderer() : W(0), H(0) {}
  void InitFrame(int w, int h) { 
     W = w, H = h;
     imageBuf.resize(W, H, 1, 3);
  }
  void SetPixel(int x, int y, float R, float G, float B);
  void SaveFrame(const char *pFileName);
  void DisplayWait();
protected:
  int W, H;
  cimg_library::CImg<BYTE> imageBuf;
};

class GLRenderer : public Renderer
{
public:
  GLRenderer(HWND hWnd);
  ~GLRenderer();
  void Resize(int W, int H);
  void Render();
  void CopyFrame(HDC hDC);
private:
  HWND hWnd;
  HGLRC hGLRC;
  HDC hDC;
};