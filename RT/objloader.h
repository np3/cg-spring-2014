#pragma once

class Mesh;
class ObjLoader
{
public:
  Mesh* createMesh(const char *pFileName);
};