#include "tracer.h"
#include "scene.h"
#include "common.h"
#include "ray.h"
#include "renderer.h"
#include <iostream>
#include <windows.h>

using namespace glm;
RayTracer::RayTracer(const Scene& s) : scn(s) {}

void RayTracer::Run(Renderer *pRenderer, int traceDepth, int resX, int resY)
{
  Camera cam;
  cam.ApplyCameraParams(scn.GetCamParams());
  cam.SetRenderPlaneDim(resX, resY);
  int W = resX;
  int H = resY;
  
  pRenderer->InitFrame(W, H);
  for (int y = 0; y < H; ++y) {
    std::cerr << "Render line " << y << std::endl;
    for (int x = 0; x < W; ++x) {
      Ray primaryRay = cam.CastRay(x, y);
      Color3 result = Trace(scn, primaryRay, 1.0f, traceDepth);
      // Make gamma correction
      const float gamma = 1.0f / 2.2f;
      result.r = pow(result.r, gamma);
      result.g = pow(result.g, gamma);
      result.b = pow(result.b, gamma);
      // Clamp result color to [0; 1] range
      result = glm::clamp(result, vec3(0), vec3(1));
      pRenderer->SetPixel(x, y, result.r, result.g, result.b);
    }
  }
}

void RayTracer::RunMT(Renderer *pRenderer, int traceDepth, int resX, int resY)
{
  Camera cam;
  cam.ApplyCameraParams(scn.GetCamParams());
  cam.SetRenderPlaneDim(resX, resY);
  int W = resX;
  int H = resY;

  pRenderer->InitFrame(W, H);

  SYSTEM_INFO si;

  GetSystemInfo(&si);
  const int numCPU = min(si.dwNumberOfProcessors, MAX_THREADS);
  std::cout << "Run ray-tracing on " << numCPU << " cores" << std::endl;
  __declspec(align(32)) ThreadData tData = {
    0, resX, resY, CreateSemaphore(0, numCPU, numCPU, 0), &scn, pRenderer, traceDepth, &cam
  };
  HANDLE th[MAX_THREADS];
  for (int i = 0; i < numCPU; ++i) {
    th[i] = CreateThread(0, 0, MTRender, &tData, 0, 0);
  }
  WaitForMultipleObjects(numCPU, th, true, INFINITE);
  for (int i = 0; i < numCPU; ++i) {
    CloseHandle(th[i]);
  }
  CloseHandle(tData.barrier);
}

inline float CalcFresnelSchlick(float NdotD, float reflAmount)
{
  float reflMin = 0.2f * reflAmount;
  return reflMin + (reflAmount - reflMin) * pow(1.0f - clamp(0.f, 1.f, NdotD), 5.0f);
}

Color3 RayTracer::TraceImpl(const Scene &scn, const Ray &primaryRay, RayIntersection *ri)
{
  RayIntersection hit(false);
  Color3 result(0);

  hit = scn.FindIntersection(primaryRay, false);
  if (!hit.exists) {
    return Color3(0);
  }
  result = scn.CalcIllumination(hit.hitShape, primaryRay, hit.dist, hit.localNormal);
  *ri = hit;
  return result;
}

Color3 RayTracer::Trace(const Scene &scn, const Ray &primaryRay, float reflectionAmount, int traceDepth)
{
  RayIntersection hit(false);
  Color3 result = TraceImpl(scn, primaryRay, &hit);

  if (!hit.exists) {
    return result;
  }
  const Material* m = hit.hitShape->GetMaterial();
  if (m->reflection < EPS) {
    return result;
  }
  reflectionAmount *= m->reflection;
  // Calc reflections
  Ray reflRay = primaryRay;
  for (int i = 0; i < traceDepth; ++i) {
    if (reflectionAmount < EPS) {
      break;
    }
    const vec3 reflDir = reflect(reflRay.Dir(), hit.localNormal);
    vec3 poi = reflRay.Apply(hit.dist);
    Ray reflectionRay(poi + float(1e-2) * reflDir, reflDir);
    float NdotD = dot(-reflDir, hit.localNormal);
    float fR = CalcFresnelSchlick(NdotD, reflectionAmount);
    result += TraceImpl(scn, reflectionRay, &hit) * reflectionAmount * fR;
    if (!hit.exists) {
      break;
    }
    
    reflectionAmount *= hit.hitShape->GetMaterial()->reflection;
    memcpy(&reflRay, &reflectionRay, sizeof(Ray));
  }
  //
  return clamp(result, Color3(0), Color3(1));
}

unsigned long __stdcall RayTracer::MTRender(void *threadData)
{
  ThreadData *tData = (ThreadData *)threadData;
  int W = tData->resX;
  int H = tData->resY;
  const int numXTiles = W / TILE_SIZE;
  const int numYTiles = H / TILE_SIZE;
  int borderX = max(0, -numXTiles * TILE_SIZE + W);
  int borderY = max(0, -numYTiles * TILE_SIZE + H);
  const int numTiles = numXTiles * numYTiles;
  Camera &cam = *tData->pCam;
  Renderer *pRenderer = tData->pRenderer;
  int traceDepth = tData->traceDepth;
  const Scene& scn = *tData->scenePtr;

  WaitForSingleObject(tData->barrier, 0);
  for (;;) {
    const int tile = InterlockedIncrement(&tData->tileID) - 1;
    if (tile >= numTiles) {
      break; // nothing to do
    }
    int tileWidth = TILE_SIZE;
    int tileHeight = TILE_SIZE;
    int xTile = (tile % numXTiles);
    int yTile = (tile / numXTiles);
    const int xa = TILE_SIZE * xTile;
    const int ya = TILE_SIZE * yTile;

    // if last tiles, add borders
    if (xTile == numXTiles - 1) {
      tileWidth += borderX;
    }
    if (yTile == numYTiles - 1) {
      tileHeight += borderY;
    }
    for (int y = 0; y < tileHeight; ++y) {
      for (int x = 0; x < tileWidth; ++x) {
        Ray primaryRay = cam.CastRay(x + xa, y + ya);
        Color3 result = Trace(scn, primaryRay, 1.0f, traceDepth);
        // Make gamma correction
        const float gamma = 1.0f / 2.2f;
        result.r = pow(result.r, gamma);
        result.g = pow(result.g, gamma);
        result.b = pow(result.b, gamma);
        // Clamp result color to [0; 1] range
        result = glm::clamp(result, vec3(0), vec3(1));
        pRenderer->SetPixel(x + xa, y + ya, result.r, result.g, result.b);
      }
    }
  }
  ReleaseSemaphore(tData->barrier, 1, NULL);
  return 1;
}
