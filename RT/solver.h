#pragma once
#include <vector>

namespace Solver {
  void SolveQuadric(const std::vector<float>& coeffs, std::vector<float>& solutions);
  void SolveCubic(const std::vector<float>& coeffs, std::vector<float>& solutions);
  void SolveQuartic(const std::vector<float>& coeffs, std::vector<float>& solutions);
}