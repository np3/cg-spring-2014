#include "light.h"
#include "ray.h"
#include "shape.h"
#include "scene.h"

using namespace glm;

inline float calcDistAtten(const glm::vec3 &dist, float range, float fadeExp) {
  float distance = length(dist);
  float distSqr = distance * distance;
  float rangeSqr = range * range;
  float dr = distance / range;
  float k = clamp(1.0f - dr * dr, 0.f, 1.f);
  return pow(k, fadeExp);
}

float Light::calcAtten(const glm::vec3 &dist) const
{
  switch(type) {
    case LIGHT_DIR:
      return 1.0f;
    case LIGHT_POINT:
      {
        return calcDistAtten(dist, range, fadeExp);
      }
    case LIGHT_SPOT:
      {
        float distAtt = calcDistAtten(dist, range, fadeExp);
        float angle = dot(normalize(dist), dir);
        float spotAtt = clamp((angle - outerCutoff) / (innerCutoff - outerCutoff), 0.0f, 1.0f);
        spotAtt = pow(spotAtt, fadeExp);

        return distAtt * spotAtt;
      }
      break;
  }
  return 1.0f;
}

Color3 Light::calcIllum(const Shape *pObj, const Scene &scn, const Ray& primaryRay, 
                        float dist, const glm::vec3 &norm) const
{
  vec3 poi = primaryRay.Apply(dist);
  vec3 distVec = pos - poi;
  float atten = calcAtten(distVec);
  const Material *m = pObj->GetMaterial();
  Color3 ambient = m->ambient() * color.ambient * atten;
  vec3 shadowRayDir;

  switch(type) {
    case LIGHT_DIR:
      shadowRayDir = dir;
      break;
    case LIGHT_POINT:
      shadowRayDir = distVec;
      break;
    case LIGHT_SPOT:
      shadowRayDir = -dir;
      break;
  }
  Ray shadowRay(poi + float(1e-3) * shadowRayDir, shadowRayDir);
  //Ray shadowRayT = shadowRay.ApplyMat(pObj->InvTransform());
  RayIntersection occlusion = scn.FindIntersection(shadowRay, true);
  if (occlusion.exists && occlusion.dist < length(distVec)) {
    return ambient;
  }
  float NdotL = dot(norm, shadowRay.Dir());
  if (NdotL < EPS) {
    return ambient;
  }
  Color3 diffuse = m->diffuse() * color.diffuse * NdotL * atten;
  Color3 specular;
  vec3 viewVec = normalize(primaryRay.Origin() - poi);
  switch (specModel) {
    case LIGHT_SPEC_PHONG:
    {
      vec3 reflVec = normalize(2.0f * dot(shadowRay.Dir(), norm) * norm - shadowRay.Dir());
      float RdotV = dot(reflVec, viewVec);
      if (RdotV < EPS) {
        return ambient + diffuse;
      }
      specular = m->specular() * color.specular * pow(RdotV, m->specPower()) * atten;
      break;
    }
    case LIGHT_SPEC_BLINN_PHONG: 
    {
      vec3 halfVec = normalize(shadowRay.Dir() + viewVec);
      float NdotH = dot(norm, halfVec);
      if (NdotH < EPS) {
        return ambient + diffuse * atten;
      }
      specular = m->specular() * color.specular * pow(NdotH, m->specPower()) * atten;
      break;
    }
  }
  return ambient + diffuse + specular;
}