#pragma once
#include <glm/glm.hpp>
#include "common.h"
#include "material.h"

enum LightType {
  LIGHT_DIR,
  LIGHT_POINT,
  LIGHT_SPOT
};

enum LightSpecularModel {
  LIGHT_SPEC_PHONG,
  LIGHT_SPEC_BLINN_PHONG
};

namespace YAML {
  class Node;
}

class Scene;
class Ray;
class Shape;
class Light
{
  friend void operator>>(const YAML::Node &node, Light &l);
public:
  Light(LightType t) : type(t), specModel(LIGHT_SPEC_PHONG), range(0.0f), dir(0.f, 0.f, 1.0f), fadeExp(1.0f) {
    if (t == LIGHT_DIR) {
      pos = glm::vec3(INFINITY, INFINITY, INFINITY);
    }
  }

  Color3 calcIllum(const Shape *pObj, const Scene &scn, const Ray& primaryRay, 
                   float dist, const glm::vec3 &norm) const;
private:
  ColorDesc color;
  LightType type;
  glm::vec3 pos;
  glm::vec3 dir;
  float innerCutoff;
  float outerCutoff;
  float range;
  float fadeExp;
  LightSpecularModel specModel;
  float calcAtten(const glm::vec3 &dist) const;
};