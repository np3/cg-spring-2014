//Petrov Nicolay
#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <string>
class Config
{
public:
  Config(const std::string& Str);
  ~Config();
  void Load();
  void* GetVariables() const;
  bool IsReady() const { return IsLoaded; }
private:
  std::string FileName;
  void *Variables;
  bool IsLoaded;
};
#endif