#include <algorithm>
#include "renderer.h"

void Renderer::SetPixel(int x, int y, float fR, float fG, float fB)
{
  BYTE R = (int)(fR * 255.0f + 0.5f);
  BYTE G = (int)(fG * 255.0f + 0.5f);
  BYTE B = (int)(fB * 255.0f + 0.5f);

  imageBuf(x, y, 0, 0) = R;
  imageBuf(x, y, 0, 1) = G;
  imageBuf(x, y, 0, 2) = B;
}

void Renderer::SaveFrame(const char *pFileName)
{
  imageBuf.save_bmp(pFileName);
}

void Renderer::DisplayWait() {
  using namespace cimg_library;
  CImgDisplay disp(imageBuf);

  while (!disp.is_closed() && !disp.is_keyESC());
}

GLRenderer::GLRenderer( HWND hWnd )
{
  int pf;
  PIXELFORMATDESCRIPTOR pfd = {0};

  hDC = GetDC(hWnd);
  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion = 1;
  pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 32;
  pfd.cAlphaBits = 8;
  pfd.cDepthBits = 24;
  pfd.cStencilBits = 8;

  pf = ChoosePixelFormat(hDC, &pfd);

  DescribePixelFormat(hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);
  SetPixelFormat(hDC, pf, &pfd);
  hGLRC = wglCreateContext(hDC);
  wglMakeCurrent(hDC, hGLRC);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
}

void GLRenderer::Resize( int w, int h )
{
  W = w;
  H = h;

  glViewport(1, 1, W, H);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void GLRenderer::Render()
{
  glClear(GL_COLOR_BUFFER_BIT);

  glColor3d(1, 1, 1);
  glEnable(GL_TEXTURE_2D);
//  glTexImage2D(GL_TEXTURE_2D, 0, 3, W, H, 0, GL_BGR_EXT, GL_UNSIGNED_BYTE, Frame);

  glBegin(GL_QUADS);
  glTexCoord2d(0, 0);
  glVertex3d(-1, 1, 0);
  glTexCoord2d(0, 1);
  glVertex3d(-1, -1, 0);
  glTexCoord2d(1, 1);
  glVertex3d(1, -1, 0);
  glTexCoord2d(1, 0);
  glVertex3d(1, 1, 0);
  glEnd();

  //glFlush();
}

void GLRenderer::CopyFrame(HDC hDC)
{
  SwapBuffers(hDC);
}

GLRenderer::~GLRenderer()
{
  wglMakeCurrent(NULL, NULL);
  wglDeleteContext(hGLRC);
  ReleaseDC(hWnd, hDC);
}
